# EukProt Db analysis

### NCBI Taxonomy ID
In some cases you may need to use the NCBI taxonomy ID for Eukprot genomes. I used this to convert the UniEuk lineage in the smalles possible ncbi taxid:

```
for id in $(cut -f1 data/meta/EukProt_included_data_sets.v03.2021_11_22.txt | awk 'NR>1'); do txid=$(grep $id data/meta/EukProt_included_data_sets.v03.2021_11_22.txt | cut -f11 | tr ';' '\n' | grep -v -w strain | taxonkit name2taxid | awk -F'\t' -v OFS='\t' '$2!=""' | tail -1); echo -e "$id\t$txid"; done > data/meta/Eukprot_closest_ncbi_taxa.tsv
```

## Downloading the genomes and annotations

Unfortunately, it's very difficult to download genomes and gff automatically. For NCBI and ensembl species is easier. For the ohters check and fille the file: `data/genome_custom.csv` with the urls of the genomes, and if possible the annotations.

Ideally, even if we dont use them the reads should be available

## Dependencies

* R packages: mamba install -c conda-forge -c bioconda r-tidyverse r-optparse r-phytools bioconductor-ggtree bioconductor-ggtreeextra
* BUSCO: mamba install -c bioconda busco=5.4.7
* mmseqs: mamba install -c bioconda mmseqs2
* OMArk: pip install omark
  

## BUSCO

# IDEAS

* maybe put the resulting plot in single directory
* Check this: https://github.com/Lcornet/GENERA/wiki/09.-Genome-quality-assessment
* you could add different clustering parameters
* Matreex but with eukprot hogs!!


# TODOs

- [ ] add unieuk taxonoy to p10k
- [ ] Cluster homogeneity, i.e. if a cluster has strameno and archeplastida maybe interesting
- [ ] better expand tree function
- [ ] add figures to rmarkdown
- [ ] add DAG to rmd
- [ ] add busco hm to dashboard
- [ ] add omark results to dashboard
- [ ] Do proper todos
- [ ] Kraken
- [ ] other olfi tool