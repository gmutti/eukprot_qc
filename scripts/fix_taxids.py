#!/usr/bin/env python

# this script may be needed if mmseqs really complain about taxids not being consecutive

from pathlib import Path
import argparse

def parse_args():
    parser=argparse.ArgumentParser(description="Newly created taxdump")
    parser.add_argument('-i', '--input', dest='input', required=True, type=str,
                    help='Input file: mnemo\ttaxonomy'),
    args=parser.parse_args()
    return args

if __name__ == '__main__':
    args=parse_args()

    taxid_mapping_dict = {}
    count = 1
    with open(Path(args.input).joinpath("names.dmp")) as fin:
        for line in fin:
            taxid = line.split("\t")[0]
            taxid_mapping_dict[taxid] = str(count)
            count += 1

    with open(Path(args.input).joinpath("names.dmp")) as fin, open(
        Path(args.input).joinpath("names.dmp.new"), "w"
    ) as fout:
        for line in fin:
            line = line.strip().split("\t")
            line[0] = taxid_mapping_dict[line[0]]
            line = "\t".join(line)
            fout.write(f"{line}\n")

    with open(Path(args.input).joinpath("nodes.dmp")) as fin, open(
        Path(args.input).joinpath("nodes.dmp.new"), "w"
    ) as fout:
        for line in fin:
            line = line.strip().split("\t")
            line[0] = taxid_mapping_dict[line[0]]
            line[2] = taxid_mapping_dict[line[2]]
            line = "\t".join(line)
            fout.write(f"{line}\n")

    Path(args.input).joinpath("names.dmp").unlink()
    Path(args.input).joinpath("nodes.dmp").unlink()
    Path(args.input).joinpath("nodes.dmp.new").rename("ictv-taxdump/nodes.dmp")
    Path(args.input).joinpath("names.dmp.new").rename("ictv-taxdump/names.dmp")