import pandas as pd

tax=pd.read_json('test/p10k/p10k_taxonomy.json', lines=True)
anno=pd.read_json('test/p10k/p10k_annotation.json', lines=True)
assembly=pd.read_json('test/p10k/p10k_assembly.json', lines=True)

df=tax.merge(anno, on=["id","sampleId"]).merge(assembly, on=["id","sampleId"])

df.to_csv("test/p10k/p10k_df.tsv", sep="\t", index=False)