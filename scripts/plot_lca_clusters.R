suppressPackageStartupMessages(suppressWarnings(library(tidyverse)))
suppressWarnings(library("optparse"))
library(ggsankey)

option_list <- list(
  make_option(c("-i", "--input"),
              type = "character", default = NULL,
              help = "annotated lca clusters", dest = "input"
  ),
  make_option(c("-o", "--output"),
              type = "character", default = NULL,
              help = "output plot", metavar = "character", dest = "output"
  )
)

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)
# opt <- NULL
# opt$input <- "results/clustered_ep/EukProt_lca_cluster.tsv"

lca_df <- read_delim(opt$input)
lca_df_grpd <- lca_df %>% 
  group_by(lca_lineage, lca_lineage_ranks) %>% 
  count()

true_order <- c("superkingdom","phylum","class","order","family","genus","species")

rank_df <- tibble("superkingdom"=character(),"phylum"=character(),
                  "class"=character(),"order"=character(),"family"=character(),
                  "genus"=character(),"species"=character())

for (idx in 1:nrow(lca_df_grpd)){
  lineage <- unlist(str_split(lca_df_grpd[idx,]$lca_lineage, ";"))
  names(lineage) <- unlist(str_split(lca_df_grpd[idx,]$lca_lineage_ranks, ";"))
  idxs <- match(names(lineage), true_order)
  ranks <- diff(idxs)
  i <- 1
  while (sum(ranks>1)>0) {
      if (ranks[i]>1){
        lineage <- append(lineage, lineage[i], i)
        names(lineage)[i+1] <- true_order[i+1]
        idxs <- match(names(lineage), true_order)
        ranks <- diff(idxs)
      }
      i <- i+1
    }
  rank_df <- bind_rows(rank_df, lineage)
  }


# rank_df$has_na <- complete.cases(rank_df)
rank_df$count <- lca_df_grpd$n

rank_df <- rank_df %>% 
  mutate(phylum=ifelse(!is.na(phylum), paste0('p_', phylum), phylum),
         class=ifelse(!is.na(class), paste0('c_', class), class),
         order=ifelse(!is.na(order), paste0('o_', order), order),
         family=ifelse(!is.na(family), paste0('f_', family), family),
         genus=ifelse(!is.na(genus), paste0('g_', genus), genus),
         species=ifelse(!is.na(species), paste0('s_', species), species)) %>% 
  arrange(phylum, class, order, family, genus, species, count)

lvls_tax <- c("Eukaryota",unique(c(unique(rank_df$phylum), unique(rank_df$class), unique(rank_df$class), 
                     unique(rank_df$order), unique(rank_df$family),unique(rank_df$genus))))

rank_df <- rank_df %>% 
  mutate(phylum=factor(phylum, ordered = T, lvls_tax),
         class=factor(class, ordered = T, lvls_tax),
         order=factor(order, ordered = T, lvls_tax),
         family=factor(family, ordered = T, lvls_tax),
         genus=factor(genus, ordered = T, lvls_tax),
         species=factor(species, ordered = T, lvls_tax))

# rank_df$angle <- ifelse(is.na(rank_df$species), 90, 0)

# rank_df <- rank_df %>%
#   rowwise() %>%
#   mutate(phylum=coalesce(phylum, superkingdom), class=coalesce(class, phylum),
#          order=coalesce(order, class), family=coalesce(family, order),
#          genus=coalesce(genus, family), species = coalesce(genus, species))

rank_df_long <- rank_df %>% 
  make_long(colnames(rank_df)[1:6], value = count) %>% 
  mutate(node=factor(node, lvls_tax), next_node=factor(next_node, lvls_tax),
         label=gsub(".*_", "", node)) %>% 
  filter(!is.na(node))

alluvial_plot <- ggplot(rank_df_long, aes(x = x, 
               next_x = next_x, 
               node = node, 
               next_node = next_node,
               fill = node, label=label)) +
  # geom_sankey(flow.alpha = 0.75, node.color = 1, type = "sankey") +
  # geom_sankey_label(size = 2.5, color = 1, fill = "aliceblue") +
  geom_alluvial(space = 2, width = .3, flow.alpha = .6) +
  geom_alluvial_label(size = 2.5, space = 2, color = 1, fill = "aliceblue") +
  # scale_fill_viridis_d() +
  theme_alluvial(base_size = 18) +
  theme(legend.position = "none", axis.text.y = element_blank(),
        axis.ticks.y = element_blank(), axis.title.x = element_blank(),
        axis.text.x = element_text(angle=0, family = "Helvetica", colour = "black"))

ggsave(opt$output, alluvial_plot, width = 12, height = 8)

