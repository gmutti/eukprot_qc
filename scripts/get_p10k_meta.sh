# Taxonomy file: https://ngdc.cncb.ac.cn/p10k/api/taxonomy/P10K-MW-000028
# Annotation statistics: https://ngdc.cncb.ac.cn/p10k/api/annotation/P10K-MW-000028
# Assembly statistics: https://ngdc.cncb.ac.cn/p10k/api/assembly/P10K-MW-000028

> p10k_taxonomy.json
> p10k_annotation.json
> p10k_assembly.json

for id in $(cut -f1 -d',' Sample_List.csv | sed 's/"//g'); do
    wget -O - https://ngdc.cncb.ac.cn/p10k/api/taxonomy/$id | sed 's/}{/}\n{/g' | sed  -e '$a\' >> p10k_taxonomy.json
    wget -O - https://ngdc.cncb.ac.cn/p10k/api/annotation/$id | sed 's/}{/}\n{/g' | sed  -e '$a\' >> p10k_annotation.json
    wget -O - https://ngdc.cncb.ac.cn/p10k/api/assembly/$id | sed 's/}{/}\n{/g' | sed  -e '$a\' >> p10k_assembly.json
done